/*
	Repetition Control Structure (Loops)
		Loops are one of the most important feature that a program must have.
		It lets us execute a code repeatedly in a pre-set number or maybe forever.
*/

/*
	Mini-activity:
		Create a function named "greeting" and display "Hello, my World!" using console.log inside a function.

		Invoke the greeting() function 20 times.
*/

function greeting() {
	console.log("Hello, my World!");
}

let count = 20;

while (count !== 0) {
	console.log("This is printed inside the loop: " + count);
	greeting();

	count--;
}

/*
	While Loop
		takes in an expression/condition

	Syntax:
		while (expression/condition) {
			statement
		}
*/

/*
	Mini-activity:
		The while loop should only display the numbers 1 - 5.
		Correct the following loop.
*/

let x = 0;

while (x < 6) {
	console.log(x);
	x++;
}

/*
	Do While Loop
		do-while loop works a lot like the while loop, but unlike the while loop, do-while loops guarantees that the code will be executed at least once

	Syntax:
		do {
			statement
		} while (expression/condition)
*/

let number = Number(prompt("Give me a number: "));

do {
	console.log("Do While: " + number);

	number += 1;
} while (number < 10)

/*
	For Loops
		more flexible than the while and do-while loop

	Syntax:
		for (initialization; expression/condition; finalExpression) {
			statement
		}
*/

for (let count = 0; count <= 20; count++) {
	console.log("For Loop:" + count);
}

//can we use for loops in string? Yes
let myString = "Aron";

accessing elements of a string
console.log(myString[0]); //A
console.log(myString[1]); //r

console.log(myString.length); //4

for (let x = 0; x < myString.length; x++) {
	console.log(myString[x]);
}

let myName = "Edwin Aron";

for (let i = 0; i < myName.length; i++) {
	if (
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u"
	) {
		console.log("Vowel!");
	} else {
		console.log(myName[i]);
	}
}

//Continue and Break Statements
/*
	"Continue" statement allows the code to go to the next iteration of the loop without finishing the execution of all the statements in a code block

	"Break" statement is used to terminate the current loop once the match has been found
*/

for (let count = 0; count <= 20; count++) {
	if (count % 2 === 0) {
		console.log("Even Number");
		continue;
	}

	console.log("Continue and Break: " + count);

	if (count > 10) {
		break;
	}
}

let name = "Alexandro";

for (let i = 0; i < name.length; i++) {
	console.log(name[i]);

	if (name[i].toLowerCase() === "a") {
		console.log("Continue to the next iteration");
		continue;
	}

	if (name[i] === "d") {
		break;
	}
}

